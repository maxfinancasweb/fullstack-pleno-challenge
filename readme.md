![Atta](https://atta.com.vc/img/img-logo-home.png)


# { "developer": "fullstack pleno" }

The objective of this challenge is to evaluate your domain in fullstack development: its organization, style and good practices with code, APIs creation, frameworks knowledge and technologies.

## Rules

1. Your code should be made available in a public or private repository, on your personal github or bitbucket;
2. Send the link to diego.vieira@atta.com.vc;
3. We use VueJS with Vuetify and C# .Net Core

## Challenge

### Front-end

The following layouts should be developed

### Layout #1
![layout](layout-cep.gif)

Create a simple CEP field component

- Use ViaCep Endpoint.

# GET: Response in JSON Format

> https://viacep.com.br/ws/{{CEP}}/json/

## Endpoint Response

```

{
 cep: "04571-010",
 logradouro: "Avenida Engenheiro Luiz Carlos Berrini",
 complemento: "até 1405 - lado ímpar",
 bairro: "Cidade Monções",
 localidade: "São Paulo",
 uf: "SP",
 unidade: "",
 ibge: "3550308",
 gia: "1004"
}
```

### Layout #2
![layout](layout-lista.jpg)

#### Steps
1. Search endpoint
2. Create a Form (cpf, Nome, Franquia, Parceiro, Consultor) to add items on list

- You can use the libs and frameworks that make you more comfortable, we suggest Vuetify but fell free to create your own components;
- All fields are required in the form;
- We'll be happy if you develop tests for it;

### Back-end

The layout has a form (a button to add).

- You need to create APIs that sends and receives these informations;
- We'll be happy if you develop tests for it;
- In case of inconsistency, return the error in a structured JSON with HTTP 400 code;

#### Plus

1. [Use Vuetify](https://vuetifyjs.com/en/getting-started/quick-start/)
2. [REST Resource Naming](https://restfulapi.net/resource-naming/)
3. [SignalR](https://dotnet.microsoft.com/apps/aspnet/signalr)

Feel free to make improviments like new funcionalities, visual optimizations, API security, etc. 😉

## Questions?

Send your questions directly to diego.vieira@atta.com.vc or opening a issue.

# May the force be with you, good luck!
